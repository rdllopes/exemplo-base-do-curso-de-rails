class Comentario
  include MongoMapper::Document

  key :autor, String
  key :texto, String

  belongs_to :post
end